#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

"""


if __name__ == "__main__":
    
    f = open("prenoms/prenoms-gars-filles-communs.txt", "r")
    lLignes = f.readlines()


    for sLigne in lLignes[1:]:
        [sPrenom, nFilles, nGars, fRation] = sLigne.split(",")
        if int(nFilles) > 50 and int(nGars) > 50 \
           and float(fRation) > 0.45 and float(fRation) < 0.55 :
            print (int(nFilles)+int(nGars), sPrenom, nFilles, nGars, fRation[:-1])


