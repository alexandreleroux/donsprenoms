#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Name:       donsprenoms.py
Description: `donsprenoms.py` est un script qui permet de relier les 
                    prénoms des donateurs aux partis politiques du Québec selon le genre, 
                    en reliant le prénom à la liste de la 
                    [banque des prénoms de Retraite Québec]
                     (https://www.donneesquebec.ca/recherche/fr/organization/rrq)

Notes:  https://framagit.org/MiguelTremblay/donsprenoms

Author: Miguel Tremblay
Date: 7 juin 2018
"""

import sys
import os
import re
import csv
import shutil
import unidecode
# From progress https://pypi.python.org/pypi/progress
import progress
from progress.bar import Bar

VERSION = "0.1"
# Verbose level:
## 1 Normal mode
## 2 Full debug
NORMAL= 1
VERBOSE= 2

nGlobalVerbeux = 1

# CSV 
COLUMN_TITLE_DGEQ=["Nom-Prenom", "Montant total", \
              "Nombre de versements" ,"Entite politique", \
              "Annee financiere"]

COLUMN_TITLE_SORTIE=["Année",\
                     "Parti politique",\
                     "Nbr donateurs", \
                     "Proportion donateurs annuel (%)", \
                     "Proportion argent annuel (%)", \
                     "Montant total", \
                     "Montant moyen",\
                     "Nbr F", \
                     "Montant F", \
                     "Montant moyen F",\
                     "Proportion donateurs F", \
                     "Proportion montants F", \
                     "Nbr H", \
                     "Montant H",\
                     "Montant moyen H",\
                     "Proportion donateurs H", \
                     "Proportion montants H", \
                     "Nbr I", \
                     "Montant I", \
                     "Montant moyen I",\
                     "Proportion donateurs I", \
                     "Proportion montant I"]


dDecompte = {"Fille" : 0, "Gars" : 0 , "Commun": 0, "Inconnu":0, \
             "CashFille":0.0, "CashGars":0.0, "CashInconnu":0.0}


def donsprenoms(sPathCSV, lFillePath, lGarsPath, sCommunPath, bAfficheInconnus,\
                fLimitSup, fLimitInf, sPathFichierSortie):
   """
   Combine les prénoms du fichier sPathCSV avec les dico qui se trouvent dans le 
   répertoire sRepertoire.

   sPathCSV: Chaîne de caractères du chemin d'accès du fichier CSV avec les donateurs.
   lFillePath: Liste des chemins d'accès des fichiers avec les prénoms féminins.
   lGarsPath: Liste des chemins d'accès des fichiers avec les prénoms masculins.   
   sCommunPath: Chaîne de caractères du chemin d'accès du fichier
   avec les prénoms féminins et masculins, de même que le nombre de chacun,
   suivi du ratio.
   bAfficheInconnus: Booleen indiquant si les prénons non reconnus doivent
   être affichés (défaut: False).
   
   fLimitSup: Float. Identifier seulement les dons au-dessous de cette valeur.
   fLimitInf: Float. Identifier seulement les dons au-dessus de cette valeur.

   sPathFichierSortie = Chemin d'accès pour écriture du fichier de sortie. 
   """
   
   # Ouverture du fichier CSV
   if os.path.exists(sPathCSV) == False:
         print("ERREUR: Fichier CSV n'existe pas: " +sPathCSV)
         exit(2)
   else:
      file_list = open(sPathCSV, 'r', encoding = "ISO-8859-1")
      liste_prenoms_donateurs = csv.DictReader(file_list, \
                                               fieldnames=COLUMN_TITLE_DGEQ, delimiter=';')


   # Set the progress bar
   columns = shutil.get_terminal_size()[0]
   lRows = list(liste_prenoms_donateurs)
   nWidth = int(columns) - 32
   bar = Bar('Traitement', max=len(lRows), width=int(nWidth), \
             suffix='%(percent)d%%')

   # Remettre la liste des fichiers à zéro:
   ##  la transformation en liste met le pointeur à la fin du fichier
   file_list.seek(0)
   liste_prenoms_donateurs = csv.DictReader(file_list, \
                                            fieldnames=COLUMN_TITLE_DGEQ, delimiter=';')

   # Chargement des dictionnaires de prenoms
   lFille = load_fichier_prenoms(lFillePath)
   lGars = load_fichier_prenoms(lGarsPath)
   dCommun = load_fichier_commun(sCommunPath)

   # Liste des prénoms
   lInconnu = []
   dAnnee = {}
   dAnneeDecompte = {}
   nTotalDonateurs  = 0
   next(liste_prenoms_donateurs, None)  # skip the headers
   for row in liste_prenoms_donateurs:
      bar.next()
      nTotalDonateurs = nTotalDonateurs + 1

      # Test si le montant est dans les bornes demandées
      fMontant = float(row["Montant total"].replace(",","."))
      if fMontant > fLimitSup:
         if  nGlobalVerbeux == VERBOSE:
            print ("\nMontant supérieure à la limite demandée", \
                   fMontant, ">", fLimitSup)
            print (row)
         continue
      elif fMontant < fLimitInf:
         if  nGlobalVerbeux == VERBOSE:
            print ("\nMontant inférieur à la limite demandée", \
                   fMontant, ">", fLimitSup)
            print (row)
         continue
         
      sPrenom = get_prenom(row)
      sParti = row["Entite politique"]
      sAnnee = row["Annee financiere"]
      
      # Montant par partie politique par annee
      ## Initialisation
      if sAnnee not in list(dAnnee):
         dCashParti = {}
         dAnnee[sAnnee]= dCashParti
         dAnneeDecompte[sAnnee] ={"Fille" : 0, "Gars" : 0 , "Commun": 0, "Inconnu":0, \
                                  "CashFille":0.0, "CashGars":0.0, "CashInconnu":0.0}
      if sParti not in list(dAnnee[sAnnee].keys()):
         dAnnee[sAnnee][sParti] = {}
         for sItem in list(dDecompte.keys()):
            dAnnee[sAnnee][sParti][sItem] = 0

      # C'est ici la recette secrete. On ajoute le ratio de fille
      #  qui a eu ce prénom aux filles
      #  et on fait la même chose pour les garçons.
      fRatio = get_ratiof(sPrenom, lFille, lGars, dCommun)

      # Si le prénom est dans une liste
      if fRatio is not None:         
         # Les filles
         dAnneeDecompte[sAnnee]["Fille"] = dAnneeDecompte[sAnnee]["Fille"] + fRatio
         dAnnee[sAnnee][sParti]['Fille'] = dAnnee[sAnnee][sParti]['Fille'] + fRatio
         dAnneeDecompte[sAnnee]["CashFille"] = dAnneeDecompte[sAnnee]["CashFille"] + fRatio*fMontant
         dAnnee[sAnnee][sParti]['CashFille'] = dAnnee[sAnnee][sParti]['CashFille'] +\
                                           fRatio*fMontant

         # Les garçons
         dAnneeDecompte[sAnnee]["Gars"] = dAnneeDecompte[sAnnee]["Gars"] + (1-fRatio)
         dAnnee[sAnnee][sParti]['Gars'] = dAnnee[sAnnee][sParti]['Gars'] + (1-fRatio)
         dAnneeDecompte[sAnnee]["CashGars"] = dAnneeDecompte[sAnnee]["CashGars"] + (1-fRatio)*fMontant
         dAnnee[sAnnee][sParti]['CashGars'] = dAnnee[sAnnee][sParti]['CashGars'] +\
                                          (1-fRatio)*fMontant

      # Sinon le prénom n'est pas dans une liste
      else:
         lInconnu.append(sPrenom)
         dAnneeDecompte[sAnnee]["Inconnu"] = dAnneeDecompte[sAnnee]["Inconnu"] + 1
         dAnneeDecompte[sAnnee]["CashInconnu"] = dAnneeDecompte[sAnnee]["CashInconnu"] + fMontant
         dAnnee[sAnnee][sParti]['CashInconnu'] = dAnnee[sAnnee][sParti]['CashInconnu'] +\
                                             fMontant
         dAnnee[sAnnee][sParti]['Inconnu'] = dAnnee[sAnnee][sParti]['Inconnu'] + 1


   bar.finish()
   
   # Bloc d'affichage #############################
   write_csv(dAnneeDecompte, dAnnee, sPathFichierSortie)

   if bAfficheInconnus:
      if len(lInconnu) == 0:
         print("Aucun prénom inconnu")
      else:
         lInconnu = sorted(set(lInconnu))
         print ("Noms inconnus:")
         print("\n".join(lInconnu))

def get_ratiof(sPrenom, lPrenomsFilles, lPrenomsGars, dPrenomCommuns):
   """
   Analyse le prenom pour retourner le ratio fille/gars qui lui correspond.
   Dans le cas d'un prenom simple, regarder dans les listes et retourner la valeur.
   Dans le cas d'un prenom compose, s'il est absent des listes,
   regarder chacune de ses composantes et retourner la moyenne des ratios trouves.
   """

   fRatio = None
   lCommun = list(dPrenomCommuns.keys())

   if len(sPrenom) == 0:
      return fRatio
   
   if sPrenom in lCommun:
      fRatio = dPrenomCommuns[sPrenom]
   elif sPrenom in lPrenomsFilles:
      fRatio = 1.0
   elif sPrenom in lPrenomsGars:
      fRatio = 0.0
   else:
      lPrenoms = re.split("\.|\-|\ |\(|\)",sPrenom)
      if "JR" in lPrenoms or "SR" in lPrenoms: # Homme
         fRatio = 0.0
         return fRatio
      elif len(lPrenoms) == 1:
         return fRatio
      # Inspection de toutes les particules
      fRatioTotal = None
      i = 0
      for sParticule in lPrenoms:
         fRatioLocal = get_ratiof(sParticule, lPrenomsFilles, lPrenomsGars, dPrenomCommuns)
         if fRatioLocal != None:
            if fRatioTotal == None:
               fRatioTotal= 0.0
            fRatioTotal = fRatioTotal + fRatioLocal
            i = i + 1
      if fRatioTotal != None:
         fRatio = fRatioTotal/i
                  

   return fRatio
   

def write_csv(dDecompteAnnee, dDecompteAnneeParti, sPath):
   """
   Écriture du fichier CSV  ou encore affichage des résultats.
   """

   lLigne = []
   lLigne.append(",".join(COLUMN_TITLE_SORTIE))
   csvfile = None
   if sPath != None:
      csvfile = open(sPath, 'w')
   else:
      print("Fichier de sortie non spécifié")

      
   lAnnee = list(dDecompteAnneeParti.keys())
   lAnnee.sort()
   for sAnnee in lAnnee:      
      dDecompteParti = dDecompteAnneeParti[sAnnee]

      # Nbr total de donateurs
      (fFilleTotal, fGarsTotal, fInconnuTotal, fNbrDonateurTotal, \
       fRatioFilleTotal, fRatioGarsTotal, fRatioInconnuTotal) = \
       get_nbr_donateurs(dDecompteAnnee[sAnnee])

      # Montant totaux
      (fArgentFilleTotal, fArgentGarsTotal, fArgentInconnuTotal, fArgentTotal,\
       fRatioArgentFilleTotal, fRatioArgentGarsTotal, fRatioArgentInconnuTotal) = \
       get_nbr_donateurs(dDecompteAnnee[sAnnee], 'Cash')

      # Calcul des montants moyens
      fMontantMoyenTotal = get_montant_moyen(fArgentTotal,fNbrDonateurTotal)
      fMontantMoyenFille = get_montant_moyen(fArgentFilleTotal,fFilleTotal)
      fMontantMoyenGars = get_montant_moyen(fArgentGarsTotal,fGarsTotal)
      fMontantMoyenInconnu = get_montant_moyen(fArgentInconnuTotal,fInconnuTotal)
      
      lLigne.append(",".join([sAnnee, # Année (A)  \
                           "Tous", # Parti politique (B) \
                           str(fNbrDonateurTotal), # Nbr donateurs (C) \
                           "1.0",   # Proportion donateurs (%) (D) \
                           "1.0",   # Proportion argent (%) (E) \
                           str(fArgentTotal), # Montant total (F) \
                           str(fMontantMoyenTotal), # Montant moyen (G) \
                           str(fFilleTotal), # Nbr F (H) \
                           str(fArgentFilleTotal), # Montant F (I) \
                           str(fMontantMoyenFille), # Montant moyen F (J) \
                           str(fRatioFilleTotal), # Proportion donateurs F (K) \
                           str(fRatioArgentFilleTotal), # Proportion montants F (L) \
                           str(fGarsTotal), # Nbr H (M) \
                           str(fArgentGarsTotal), # Montant H (N) \
                           str(fMontantMoyenGars),  # Montant moyen H (O)\
                           str(fRatioGarsTotal), # Proportion donateurs H (P)\
                           str(fRatioArgentGarsTotal), # Proportion montants H (Q) \
                           str(fInconnuTotal), # Nbr I (R) \
                           str(fArgentInconnuTotal), # Montant  I (S) \
                           str(fMontantMoyenInconnu), # Montant moyen I (T) \
                           str(fRatioInconnuTotal), # Proportion donateurs I (U) \
                           str(fRatioArgentInconnuTotal) # Proportion montant I (V)
                           ])) 




      
      # Pour chaque parti politique
      lPartis  = list(dDecompteParti)
      lPartis.sort()
      for sParti in lPartis:
         # Nombre de donateurs
         (fNbrFilleParti, fNbrGarsParti, nNbrInconnuParti, fNbrDonateurParti, \
          fRatioFilleParti, fRatioGarsParti, fRatioInconnuParti) =  \
          get_nbr_donateurs(dDecompteParti[sParti])

         # Montant
         (fArgentFilleParti, fArgentGarsParti, fArgentInconnuParti, fArgentTotalParti, \
          fRatioArgentFilleParti, fRatioArgentGarsParti, fRatioArgentInconnuParti) = \
          get_nbr_donateurs(dDecompteParti[sParti], 'Cash')


         # Moyenne
         fRatioDonateurParti = get_montant_moyen(fNbrDonateurParti,fNbrDonateurTotal)
         fRatioArgentTotalParti = get_montant_moyen(fArgentTotalParti,fArgentTotal)
         fRatioMontantMoyenParti = get_montant_moyen(fArgentTotalParti, fNbrDonateurParti)
         fRatioMontantMoyenFilleParti =get_montant_moyen(fArgentFilleParti, fNbrFilleParti)
         fRatioMontantMoyenGarsParti =get_montant_moyen(fArgentGarsParti, fNbrGarsParti)
         fRatioMontantMoyenInconnuParti = \
                                        get_montant_moyen(fArgentInconnuParti,nNbrInconnuParti )

         lLigne.append(",".join([sAnnee, # Année (A)  \
                           sParti, # Parti politique (B) \
                           str(fNbrDonateurParti), # Nbr donateurs (C) \
                           str(fRatioDonateurParti),  # Proportion donateurs (%) (D) \
                           str(fRatioArgentTotalParti),   # Proportion argent (%) (E) \
                           str(fArgentTotalParti), # Montant total (F) \
                           str(fRatioMontantMoyenParti), # Montant moyen (G) \
                           str(fNbrFilleParti), # Nbr F (H) \
                           str(fArgentFilleParti), # Montant F (I) \
                           str(fRatioMontantMoyenFilleParti), # Montant moyen F (J) \
                           str(fRatioFilleParti), # Proportion donateurs F (K) \
                           str(fRatioArgentFilleParti), # Proportion montants F (L) \
                           str(fNbrGarsParti), # Nbr H (M) \
                           str(fArgentGarsParti), # Montant H (N) \
                           str(fRatioMontantMoyenGarsParti),  # Montant moyen H (O)\
                           str(fRatioGarsParti), # Proportion donateurs H (P)\
                           str(fRatioArgentGarsParti), # Proportion montants H (Q) \
                           str(nNbrInconnuParti), # Nbr I (R) \
                           str(fArgentInconnuParti), # Montant  I (S) \
                           str(fRatioMontantMoyenInconnuParti), # Montant moyen I (T) \
                           str(fRatioInconnuParti), # Proportion donateurs I (U)
                           str(fRatioArgentInconnuParti) # Proportion montant I (V)
                           ])) 




   if csvfile != None:
      csvfile.writelines([sString+os.linesep for sString in lLigne])
      csvfile.close()
   else:
      for sLigne in lLigne:
         print (sLigne)

def get_montant_moyen(fArgent, fDonateur):
   """
   Retourne le montant moyen pour le nombre de donateur. Verifie si le nombre de
   donateurs est different de zero, si c'est le cas retourner 'n/a'.
   """
   
   if fDonateur != 0:
      fMoyen = round(fArgent/fDonateur,4)
   else:
      fMoyen = "n/a"                        

   return fMoyen
   
def get_nbr_donateurs(dDecompte, sKey=""):
   """
   Calcule les valeurs totales et ratio pour le nombre gars, filles et inconnus
   et retourne le tout dans un tuple.

   sKey est un préfixe à la clé du dictionnaire qui permet d'aller
   chercher les montants lorsqu'il prend la valeur de 'Cash'.
   """

   # Nbr total de donateurs
   fFilleTotal = round(dDecompte[sKey + "Fille"],2)
   fGarsTotal = round(dDecompte[sKey + "Gars"],2)
   fInconnuTotal = round(dDecompte[sKey + "Inconnu"],2)
   fNbrDonateurTotal =  fFilleTotal + fGarsTotal + fInconnuTotal

   if fNbrDonateurTotal != 0:
      fRatioFilleTotal = round(fFilleTotal/fNbrDonateurTotal,4)
      fRatioGarsTotal = round(fGarsTotal/fNbrDonateurTotal,4)
      fRatioInconnuTotal = round(fInconnuTotal/fNbrDonateurTotal,4)
   else:
      fRatioFilleTotal = "n/d"
      fRatioGarsTotal = "n/d"
      fRatioInconnuTotal = "n/d"

   return (fFilleTotal, fGarsTotal, fInconnuTotal, fNbrDonateurTotal, \
           fRatioFilleTotal, fRatioGarsTotal, fRatioInconnuTotal)
      

def get_prenom(ligne):
   """
   Fait une liste de vérifications pour retourner un prénom dépouillé et vérifié.
   On prend la 'ligne' complète du fichier CSV en argument, afin de pouvoir écrire
   de l'information en cas de truc étrange.
   """

   sNomPrenom = ligne["Nom-Prenom"]
   
   sPrenomAccent = sNomPrenom.split(",")[-1].strip().upper()
   # Retrait des accents
   sPrenom = unidecode.unidecode(sPrenomAccent)

   # Prénom vide?
   nLengthPrenom = len(sPrenom)
   if  nLengthPrenom == 0 or nLengthPrenom == 1:
      if nGlobalVerbeux == VERBOSE:
         print ("\nAVERTISSEMENT: Prénom vide ou une lettre", sPrenom)
         print (ligne)
      return sPrenom
   ### Dé du prénom ###
   # Est-ce que le prénom drébute par une lettre suivi d'un point?
   if sPrenom[1] == "." :
      if nLengthPrenom == 2 or (nLengthPrenom == 4 and  sPrenom[3] == "."):
         if nGlobalVerbeux == VERBOSE:
            print ("\nAVERTISSEMENT: Prénom une lettre", sPrenom)
            print (ligne)
         return sPrenom
      elif sPrenom[0].isalpha():
         # Cas "G.B. OKILL"
         if sPrenom[2].isalpha() and sPrenom[3] == ".":
            sPrenom = sPrenom[4:].strip().strip("-")
         # Cas "J. BRIAN" and "J.GERARD" and " J.-AIME"
         else: 
            sPrenom = sPrenom[2:].strip().strip("-")

   ### Fin du prénom ###
   # Est-ce que le prénom se termine par une lettre suivi d'un point?
   if sPrenom[-1]  == "." or False:
      if nLengthPrenom == 2:
         if nGlobalVerbeux == VERBOSE:
            print ("\nAVERTISSEMENT: Prénom une lettre", sPrenom)
            print (ligne)
         return sPrenom
      elif sPrenom[-2].isalpha():
         if sPrenom[-3] in [" ", "-"]:
            sPrenom = sPrenom[:-3].strip()
         # Cas "Diane M.J."
         elif nLengthPrenom > 5 and sPrenom [-3] in [".","-"]:
            if sPrenom[-4].isalpha() and sPrenom[-5] == " ":
               sPrenom = sPrenom[:-5].strip()
              
   # Cas "Louis-T" qui devient "Louis"
   elif sPrenom[-2] == "-":
         sPrenom = sPrenom[:-3].strip()


   return sPrenom


   
      
def load_fichier_commun(sPath):
   """
   Charge le fichier des prénoms communs aux gars et aux filles. 
   Ce fichier à la forme:
   Prénom, # Filles, # Gars, Fille vs Gars (%)

   Mettre le tout dans un dictionnaire, avec le prénom comme clé, et un Tuple avec les 
   ratio comme élément.

   Retourne le dictionnaire.
   """

   dCommun = {}

   if sPath == None:
      return dCommun
   
   if os.path.exists(sPath) == False:
      print("ERREUR: Fichier de prenoms communs n'exite pas: " +sPath)
      exit(2)
   else:
      f = open(sPath, 'r', encoding = "ISO-8859-1")
      lListe = f.read().splitlines()
      f.close()


   for sLigne in lListe[1:]: # On saute la première ligne qui est l'entête.
      lLigne = sLigne.split(",")
      sPrenom = lLigne[0]
      fRatio = float(lLigne[-1])
      dCommun[sPrenom] = fRatio

   return dCommun
         
def load_fichier_prenoms(lPath):
   """
   Chargement d'une liste de fichiers de prenoms. Fait une concatenation des différents listes.

   Retourne la liste des prénoms
   """

   lListeComplete = []

   if lPath == None:
      return lListeComplete

   for sPath in lPath:
      if os.path.exists(sPath) == False:
         print("ERREUR: Fichier de prenoms n'exite pas: " +sPath)
         exit(2)
      else:
         f = open(sPath, 'r', encoding = "ISO-8859-1")
         lListe = f.read().splitlines()
         # Transformation en majuscule et retrait des accents
         for i in range(len(lListe)):
            sPrenomAccent = lListe[i].upper()
            sPrenom = unidecode.unidecode(sPrenomAccent)
            lListe[i] = sPrenom
         f.close()
      lListeComplete = lListeComplete + lListe

   return lListeComplete
         
   
import argparse

def get_command_line():
   """
   Parse the command line and perform all the checks.
   """

   parser = argparse.ArgumentParser(prog='PROG', prefix_chars='-',\
                                    description=" relier les prénoms des donateurs aux partis politiques du Québec selon le genre, en reliant le prénom à la liste de la banque des prénoms de Retraite Québec.")

   parser.add_argument("--fichier-sortie", "-o", dest="sOutputFile", metavar=("CHEMIN"), \
                       help="Fichier de sortie CSV où seront inscrit les résultats de l'analyse. Si aucune valeur n'est indiquée, le résultat est inscrit dans le terminal",\
                       action="store", type=str, default=None)
   
   parser.add_argument("--DGEQ", "-d", dest="sDgeqPath", metavar=("CHEMIN"), \
                       help="Fichier CSV contenant les donateurs à un parti politique",\
                       action="store", type=str, default=None, required=True)
   
   parser.add_argument("--Fille", "-f", dest="sFilleListePath", nargs="*", metavar=("CHEMIN"), \
                       help="Fichier(s) contenant les prénoms des filles",\
                       action="store", type=str, default=None, required=False)
   
   parser.add_argument("--Gars", "-g", dest="sGarsListePath", nargs="*",  metavar=("CHEMIN"),\
                       help="Fichier(s) contenant les prénoms des gars",\
                       action="store", type=str, default=None, required=False)
   
   parser.add_argument("--Commun", "-c", dest="sCommunListePath",  metavar=("CHEMIN"), \
                       help="Fichier contenant les prénoms communs aux filles et aux gars",\
                       action="store", type=str, default=None, required=False)

   parser.add_argument("--Inconnu", "-i", dest="bAfficheInconnus",  \
                       help="Afficher les prénoms inconnus",\
                         action="store_true", default=False)

   parser.add_argument("--limite-sup", "-s", dest="fLimitSup", metavar=("Montant ($)"),  \
                       help="Compter seulement les dons au-dessous de ce montant",\
                         action="store", type=float, default=float("inf"))
   parser.add_argument("--limite-inf", "-I", dest="fLimitInf", metavar=("Montant ($)"),  \
                       help="Compter seulement les dons au-dessus de ce montant",\
                         action="store", type=float, default=0.0)

   parser.add_argument("--verbose", "-v", dest="bVerbeux",  \
                       help="Afficher plus d'information",\
                         action="store_true", default=False)

   parser.add_argument("--version", "-V", dest="bVersion", \
                       help="Écrire le numéro de version et sortir.",\
                       action="store_true", default=False)       

   
   # Parse the args
   options = parser.parse_args()

   if options.bVersion:
      print ("donsprenoms.py version: " + VERSION)
      print ("Copyright (C) 2018 Free Software Foundation, Inc.")
      print ("License GPLv3+: GNU GPL version 3 ou ultérieure <http://gnu.org/licenses/gpl.html>.")
      print ("Ceci est un logiciel libre ; vous êtes libre de le modifier et de le redistribuer.")
      print ("Aucune garantie n'est fournie, dans la mesure de ce que la loi autorise..\n")
      print ("Écrit par Miguel Tremblay, http://ptaff.ca/miguel/")
      exit(0)
   
  # Set the global verbosity
   global nGlobalVerbeux
   if options.bVerbeux:
      nGlobalVerbeux = VERBOSE
   else:
      nGlobalVerbeux = NORMAL

#   if options.sOutputFile != None:
      # also works when file is a link and the target is writable
#      if not os.access(options.sOutputFile, os.W_OK):
#         print ("ERREUR: fichier de sortie impossible en ecriture:", options.sOutputFile)
#         exit (10)
   
   return options
      

   
if __name__ == "__main__":

   tOptions = get_command_line()
   donsprenoms(tOptions.sDgeqPath, tOptions.sFilleListePath, tOptions.sGarsListePath, \
               tOptions.sCommunListePath, tOptions.bAfficheInconnus, \
               tOptions.fLimitSup, tOptions.fLimitInf, tOptions.sOutputFile)
   
